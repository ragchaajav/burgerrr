import React from 'react';
import css from './style.module.css';
import Shadow from '../Shadow';
// import shadow from './'

const Modal = props => 
<div>
<Shadow show = {props.show} closeConfirmModal={props.closeConfirmModal} />
<div  style={ {transform: props.show ? 'translateY(0)' : 'translateY(-1000vh)', opacity: props.show ? '1' : '0'} } className={css.Modal}>Modal ...{props.children} </div>
</div>;

export default Modal;