import React, { Component } from "react";
import Modal from "../../components/General/Modal";

import Burger from "../../components/Burger";
import BuildControls from "../../components/BuildControls";
import OrderSummary from "../../components/OrderSummary";
const INGREDIENT_PRICES = {
  salad:150,cheese:250,
  bacon:800, meat:1500
}
const INGREDIENT_NAMES = {
  bacon:'Гах мах',
  cheese: 'Бяслаг',
  meat: 'Үхрийн мах',
  salad: 'Салад'
}

class BurgerBuilder extends Component {
  state = {
    ingredients: {
      salad: 0,
      cheese: 0,
      bacon: 0,
      meat: 0,
    },
      totalPrice:1000,
      purchasing:false,
      confirmOrder:false
  };

  continueOrder = ()=>{
    console.log('continue дарагдлаа');
  }
  showConfirmModal = ()=>{
    this.setState({confirmOrder: true})
  }

  closeConfirmModal = ()=>{
    this.setState({confirmOrder: false})
  }


  ortsNemeh = (type) => {
    const newIngredients = { ...this.state.ingredients };
    newIngredients[type]++;

    const newPrice = this.state.totalPrice+ INGREDIENT_PRICES[type];

    this.setState({ purchasing:true, totalPrice: newPrice, ingredients: newIngredients });
  };

  ortsHasah = (type) => {
    if (this.state.ingredients[type] > 0) {
      const newIngredientshasah = { ...this.state.ingredients };
      newIngredientshasah[type]--;
      const newPrice = this.state.totalPrice- INGREDIENT_PRICES[type];

      this.setState({ purchasing: newPrice > 1000,totalPrice: newPrice,ingredients: newIngredientshasah });
    }
  };

  render() {
    const disabledIngredients = { ...this.state.ingredients };
    for (let key in disabledIngredients) {
      disabledIngredients[key] = disabledIngredients[key] <= 0;
    }
    return (
      <div>
        <Modal closeConfirmModal={this.closeConfirmModal} show={this.state.confirmOrder}>
          <OrderSummary 
          onCancel = {this.closeConfirmModal}
          onContinue ={this.continueOrder}
          price={this.state.totalPrice} ingredients={this.state.ingredients} ingredientNames ={INGREDIENT_NAMES} />
        </Modal>
        <Burger orts={this.state.ingredients} />
        {/* <div> Орцны удирдлага</div> */}
        <BuildControls
        showConfirmModal ={this.showConfirmModal}
        ingredientNames ={INGREDIENT_NAMES}
        disabled ={!this.state.purchasing}
        price ={this.state.totalPrice}
          disabledIngredients={disabledIngredients}
          ortsNemeh={this.ortsNemeh}
          ortsHasah={this.ortsHasah}
        />
      </div>
    );
  }
}

export default BurgerBuilder;
