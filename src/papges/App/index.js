import React from "react";
import "./style.css";
import Toolbar from "../../components/Toolbar";
import BurgerPage from "../BurgerPage";
function App() {
  return (
    <div>
      <Toolbar /> <br />
      <main className="Content">
        <BurgerPage />
      </main>
    </div>
  );
}

export default App;
